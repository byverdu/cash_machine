// SCSS gulp task

const gulp = require( 'gulp' );
const livereload = require( 'gulp-livereload' );
const config = require( '../src/config/config' );
const scss = require( 'gulp-scss' );

module.exports = function () {
  return gulp.src( config.paths.scss.src )
    .pipe( scss())
    .pipe( gulp.dest( config.paths.scss.dest ))
    .pipe( livereload());
};
