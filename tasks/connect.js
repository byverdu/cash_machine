// Task to run local server
require( 'gulp' );
const connect = require( 'gulp-connect' );

module.exports = function () {
  return connect.server({
    root: 'dist'
  });
};
