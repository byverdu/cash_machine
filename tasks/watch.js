// gulp task for watching files

const gulp = require( 'gulp' );
const livereload = require( 'gulp-livereload' );
const config = require( '../src/config/config' );

module.exports = function () {
  livereload.listen();
  gulp.watch( config.paths.scss.src, ['scss']);
  gulp.watch( config.paths.js.src, ['js', 'browserify']);
};
