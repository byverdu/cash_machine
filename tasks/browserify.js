// browserify gulp task
const gulp = require( 'gulp' );
const browserify = require( 'browserify' );
const source = require( 'vinyl-source-stream' );
const config = require( '../src/config/config' );

module.exports = function () {
  return browserify( config.paths.browserify.src )
    .bundle()
    // name of the file
    .pipe( source( config.paths.browserify.source ))
    .pipe( gulp.dest( config.paths.browserify.dest ));
};
