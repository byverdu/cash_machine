// javascript gulp Task

const gulp = require( 'gulp' );
const livereload = require( 'gulp-livereload' );
const config = require( '../src/config/config' );

module.exports = function () {
  return gulp.src( config.paths.js.src )
  .pipe( livereload());
};
