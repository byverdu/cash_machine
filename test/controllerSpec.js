/* global inject,expect */
// Tests for controller

describe( 'MainController', () => {
  beforeEach( module( 'cashMachine' ));
  let $controller;
  let $scope;
  let $utils;
  let $service;
  const data = [
    { id: '1p',
      value: 0.01,
      count: 100 },
    { id: '2p',
      value: 0.02,
      count: 100 },
    { id: '5p',
      value: 0.05,
      count: 100 },
    { id: '10p',
      value: 0.10,
      count: 100 },
    { id: '20pence',
      value: 0.20,
      count: 100 },
    { id: '50p',
      value: 0.50,
      count: 100 },
    { id: '1pound',
      value: 1,
      count: 100 },
    { id: '2pound',
      value: 2,
      count: 100 },
    { id: '5pound',
      value: 5,
      count: 50 },
    { id: '10pound',
      value: 10,
      count: 50 },
    { id: '20pound',
      value: 20,
      count: 50 },
    { id: '50pound',
      value: 50,
      count: 50 }
  ];

  beforeEach( inject( function ( _$rootScope_, _$controller_, _utils_, _service_ ) {
    $scope = _$rootScope_.$new();
    $controller = _$controller_( 'MainController', { $scope });
    $utils = _utils_;
    $service = _service_;
  }));

  describe( 'MainController', () => {
    it( 'is defined', () => {
      expect( $controller ).not.eq( undefined );
    });
    it( 'should have a title', () => {
      expect( $scope.title ).not.eq( undefined );
    });
    it( 'should have a cashContainer property', () => {
      expect( $scope.cashContainer ).not.eq( undefined );
    });
    it( 'should have displayTotalSum method', () => {
      expect( $scope.displayTotalSum ).to.be.a( 'Function' );
    });
    it( 'displayTotalSum, should return a String', () => {
      expect( $scope.displayTotalSum()).to.be.a( 'String' );
    });
    it( 'should have giveOnlyNotesOf method', () => {
      expect( $scope.giveOnlyNotesOf ).to.be.a( 'Function' );
    });
  });
});
