/* global expect, inject*/
// Spec for utils methods
const data = [
  { id: '1p',
    value: 0.01,
    count: 100 },
  { id: '2p',
    value: 0.02,
    count: 100 },
  { id: '5p',
    value: 0.05,
    count: 100 },
  { id: '10p',
    value: 0.10,
    count: 100 },
  { id: '20pence',
    value: 0.20,
    count: 100 },
  { id: '50p',
    value: 0.50,
    count: 100 },
  { id: '1pound',
    value: 1,
    count: 100 },
  { id: '2pound',
    value: 2,
    count: 100 },
  { id: '5pound',
    value: 5,
    count: 50 },
  { id: '10pound',
    value: 10,
    count: 50 },
  { id: '20pound',
    value: 20,
    count: 50 },
  { id: '50pound',
    value: 50,
    count: 50 }
];


describe( 'cashMachine Utils Service', () => {
  beforeEach( module( 'cashMachine' ));
  let $utils;

  beforeEach( inject( function( _utils_ ) {
    $utils = _utils_;
  }));

  describe( '$utils', () => {
    it( 'is defined', () => {
      expect( $utils ).not.eq( undefined );
    });
    it( 'should have a getTotalSum method', () => {
      expect( $utils.getTotalSum ).to.be.a( 'Function' );
    });
    it( 'getTotalSum, should return a Number', () => {
      expect( $utils.getTotalSum( data )).to.be.a( 'Number' );
    });
    it( 'getTotalSum, should return the total amount of money', () => {
      expect( $utils.getTotalSum( data )).to.eq( 4638 );
    });
    it( 'should have a getOnlyNotesForValue method', () => {
      expect( $utils.getOnlyNotesForValue ).to.be.a( 'Function' );
    });
    it( 'getOnlyNotesForValue, should return an Object with id and count properties', () => {
      const withdraw = 20;
      expect( $utils.getOnlyNotesForValue( '20pound', data, withdraw )).to.be.an( 'Object' )
        .with.keys(['id', 'countNeeded', 'output']);
    });
    it( 'getOnlyNotesForValue, should return the highest number of £20 notes possible, £20', () => {
      const withdraw = 20;
      expect( $utils.getOnlyNotesForValue( '20pound', data, withdraw ).countNeeded ).to.eq( 1 );
    });
    it( 'getOnlyNotesForValue, should return the highest number of £20 notes possible, £40', () => {
      const withdraw = 40;
      expect( $utils.getOnlyNotesForValue( '20pound', data, withdraw ).countNeeded ).to.eq( 2 );
    });
    it( 'getOnlyNotesForValue, should return the highest number of £20 notes possible, £50', () => {
      const withdraw = 50;
      expect( $utils.getOnlyNotesForValue( '20pound', data, withdraw ).countNeeded ).to.eq( 2 );
    });
    it( 'getOnlyNotesForValue, returns the highest number of £20 notes possible, £250', () => {
      const withdraw = 250;
      expect( $utils.getOnlyNotesForValue( '20pound', data, withdraw ).countNeeded ).to.eq( 12 );
    });
    it( 'getOnlyNotesForValue, returns the highest number of £20 notes possible, £180', () => {
      const withdraw = 180;
      expect( $utils.getOnlyNotesForValue( '20pound', data, withdraw ).countNeeded ).to.eq( 9 );
    });
    it( 'getOnlyNotesForValue, should return the highest number of £20 notes possible, £10', () => {
      const withdraw = 10;
      expect( $utils.getOnlyNotesForValue( '20pound', data, withdraw ).countNeeded ).to.eq( 0 );
    });
    it( 'should have a subtractCountFromTotal method', () => {
      expect( $utils.subtractCountFromTotal ).to.be.a( 'Function' );
    });
    it( 'subtractCountFromTotal, returns the new count for that withdraw, £60', () => {
      const withdraw = { id: '20pound', countNeeded: 3 };
      $utils.subtractCountFromTotal( data, withdraw );
      expect( data[ 10 ].count ).to.eq( 47 );
    });
    it( 'subtractCountFromTotal, returns the new count for that withdraw, £60', () => {
      const withdraw = { id: '50pound', countNeeded: 5 };
      $utils.subtractCountFromTotal( data, withdraw );
      expect( data[ 11 ].count ).to.eq( 45 );
    });
  });
});
