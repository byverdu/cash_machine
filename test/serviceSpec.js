/* global inject,expect */

// Tests for service

describe( 'cashMachine http Service', () => {
  beforeEach( module( 'cashMachine' ));
  let $httpBackend;
  let $service;
  const dataResult = {
    cashContainer: [
      {
        id: '1 pence',
        value: 0.01,
        count: 100
      },
      {
        id: '2 pence',
        value: 0.02,
        count: 100
      }
    ]
  };

  beforeEach( inject( function ( _$httpBackend_, _service_ ) {
    $httpBackend = _$httpBackend_;
    $httpBackend.when( 'GET', './data/cash_available.json' )
      .respond( 200, dataResult );
    $service = _service_;
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe( '$service', () => {
    it( 'is defined', () => {
      expect( $service ).not.eq( undefined );
    });
    it( 'should have a getCashAvailable method', () => {
      expect( $service.getCashAvailable ).to.be.a( 'Function' );
    });
    it( 'getCashAvailable, should do an http request', () => {
      $service.getCashAvailable();
      $httpBackend.expectGET( './data/cash_available.json' );
      $httpBackend.flush();
    });
    it( 'getCashAvailable, should retrieve the coins and notes objects', () => {
      $service.getCashAvailable().then( response => {
        expect( response.data ).to.have.property( 'cashContainer' )
          .that.is.an( 'Array' )
          .with.length.above( 1 );
      });
      $httpBackend.flush();
    });
  });
});
