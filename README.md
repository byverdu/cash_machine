# Cash Machine

## Description

There is a special cash machine, without any security, that dispenses money (notes and coins). The

machine has a given initial state of what coins and notes it has available.

The initial state is: 100x1p, 100x2p, 100x5p, 100x10p, 100x20p, 100x50p, 100x£1, 100x£2, 50x£5,

50x£10, 50x£20, 50x£50.

You should create a program that is given the value to withdraw as an input.

Program the cash machine, so it has 2 algorithms that can be swapped (swapping can be done by

rebuilding and rerunning the application):

1. Algorithm that returns least number of items (coins or notes)

2. Algorithm that returns the highest number of £20 notes possible

Output the number of coins and notes given for each withdrawal.

The machine should output the count and value of coins and notes dispensed and the balance amount left.

### How to install
```bash
> git clone https://byverdu@bitbucket.org/byverdu/cash_machine.git
> cd cash_machine
> npm install
```

### How to run the tests
```bash
> cd cash_machine
> karma start
```

### How to run the application for development
```bash
# After install all the npm dependencies,
> cd cash_machine
> gulp
# Visit http:localhost:8080
```

`npm install` can take some time and depends on node.js. This repo already contains a bundled and functional version in the `dist/` folder. You will need to run a local web server in order to avoid `CORS` errors.
