module.exports = {
  paths: {
    js: {
      src: ['src/js/app.js', 'src/js/**/*.js', 'gulpfile.js'],
      dest: ''
    },
    scss: {
      src: 'src/scss/*.scss',
      dest: 'dist/css'
    },
    browserify: {
      src: 'src/js/app.js',
      source: 'main.js',
      dest: 'dist/js/'
    }
  }
};
