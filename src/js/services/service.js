// http service to return the available money

module.exports = function ( $http ) {
  return {
    getCashAvailable() {
      return $http({
        method: 'GET',
        url: './data/cash_available.json'
      });
    }
  };
};
