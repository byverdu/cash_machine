// helpers methods

module.exports = function () {
  // Reduces and array to get the total amount of money
  const getTotalSum = function ( cashContainer ) {
    return cashContainer.reduce(( prevItem, nextItem ) => {
      const valueForId = ( nextItem.value * nextItem.count );
      const result = prevItem += valueForId;

      return result;
    }, 0 );
  };
  // Calculates count of £20 pounds notes
  const getOnlyNotesForValue = function ( notesValue, cashContainer, withdraw ) {
    const notes = cashContainer.find( currency => currency.id === notesValue );
    const countNeeded = ( withdraw / notes.value );

    return {
      id: notesValue,
      countNeeded: Math.floor( countNeeded ),
      output: notes.output
    };
  };
  // Helper to create a sorted array containing the count needed
  const createNeededCountSortedArray = function ( array, scope ) {
    const tempContainer = [];
    array.forEach( element => {
      const tempObject = {
        id: element.id,
        value: element.value,
        count: element.count,
        countNeeded: Math.floor( scope / element.value ),
        output: element.output
      };
      tempContainer.push( tempObject );
    });
    return tempContainer.sort( function ( a, b ) {
      return a.countNeeded - b.countNeeded;
    });
  };
  // Subtracts the count needed for the withdraw within that note or coin
  const subtractCountFromTotal = function ( cashContainer, withdrawContainer ) {
    const cashItem = cashContainer.find( item => item.id === withdrawContainer.id );
    cashItem.count = cashItem.count - withdrawContainer.countNeeded;
  };

  return {
    getTotalSum,
    getOnlyNotesForValue,
    subtractCountFromTotal,
    createNeededCountSortedArray
  };
};
