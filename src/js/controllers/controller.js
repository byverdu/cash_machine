module.exports = function ( $scope, utils, service ) {
  $scope.cashContainer = [];

  service.getCashAvailable().then( response => {
    angular.forEach( response.data.cashContainer, currency => {
      $scope.cashContainer.push( currency );
    });
  });

  $scope.title = 'Cash Machine';
  $scope.isSubmited = false;
  $scope.isSubmitedValid = false;

  $scope.submitWithdraw = function () {
    $scope.isSubmited = true;
    // Conditon for if statement
    const isEmptyWithdraw = $scope.withdraw === undefined || isNaN( Number( $scope.withdraw )) || $scope.withdraw === '';

    if ( isEmptyWithdraw ) {
      $scope.isSubmitedValid = false;
      $scope.withdrawMessage = 'Sorry your request is not possible';
      return;
    }

    const sortedContainer = utils.createNeededCountSortedArray( $scope.cashContainer, $scope.withdraw );
    const biggerThanOne = sortedContainer.find( item => item.countNeeded >= 1 );
    // Conditon for if statement
    const isModEqualZero = ( $scope.withdraw % biggerThanOne.value ) === 0;
    const countNeededContainer = [];

    if ( isModEqualZero ) {
      countNeededContainer.push( biggerThanOne );
      $scope.isSubmitedValid = true;
      $scope.displayCountResult = countNeededContainer;
      utils.subtractCountFromTotal( $scope.cashContainer, biggerThanOne );
      $scope.newAmountMoney = $scope.displayTotalSum();
    } else {
      countNeededContainer.push( biggerThanOne );
      const countByValue = ( biggerThanOne.countNeeded * biggerThanOne.value );
      // Math.round to be sure that we always get a whole number
      let innerWithdraw = Math.round(( $scope.withdraw - countByValue ) * 100 ) / 100;

      while ( innerWithdraw > 0 ) {
        // sort by count needed and get the first item to subtract till withdraw is 0
        const sorted = utils.createNeededCountSortedArray( $scope.cashContainer, innerWithdraw );
        const bigger = sorted.find( item => item.countNeeded >= 1 );
        const innerCountByValue = ( bigger.countNeeded * bigger.value );

        // Math.round to be sure that we always get a whole number
        innerWithdraw = Math.round(( innerWithdraw - innerCountByValue ) * 100 ) / 100;
        countNeededContainer.push( bigger );
      }

      // creating output and subtract count from total
      countNeededContainer.forEach( item => {
        utils.subtractCountFromTotal( $scope.cashContainer, { id: item.id, countNeeded: item.countNeeded });
      });
      $scope.isSubmitedValid = true;
      $scope.displayCountResult = countNeededContainer;
      $scope.newAmountMoney = $scope.displayTotalSum();
    }
  };


  $scope.displayTotalSum = function () {
    const newTotal = utils.getTotalSum( $scope.cashContainer ).toFixed( 2 );
    return `£ ${newTotal}`;
  };

  $scope.giveOnlyNotesOf = function () {
    const count20 = utils.getOnlyNotesForValue( '20pound', $scope.cashContainer, $scope.withdraw );

    $scope.withdrawMessage = '';
    $scope.isSubmited = true;

    if ( isNaN( count20.countNeeded ) || count20.countNeeded === 0 ) {
      $scope.withdrawMessage = 'Sorry your request is not possible';
      $scope.isSubmitedValid = false;
    } else {
      $scope.isSubmitedValid = true;
      $scope.displayCountResult = [count20];
      utils.subtractCountFromTotal( $scope.cashContainer, count20 );
      $scope.newAmountMoney = $scope.displayTotalSum();
    }
  };
};
