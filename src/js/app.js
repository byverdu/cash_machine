// Entry point for the app

const angular = require( 'angular' );
const MainController = require( './controllers/controller' );
const service = require( './services/service' );
const utils = require( './services/utils' );

angular.module( 'cashMachine', [])
  .factory( 'utils', [utils])
  .factory( 'service', ['$http', service])
  .controller( 'MainController', ['$scope', 'utils', 'service', MainController]);
